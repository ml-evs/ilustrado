.. include:: readme.rst

.. toctree::
    :hidden:
    :maxdepth: 2
    
    Python API <modules>
